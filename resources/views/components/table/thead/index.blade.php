<thead {{ $attributes->whereDoesntStartWith('tr') }}>
<tr {{ $attributes->thatStartWith('tr') }}>
  {{ $slot }}
</tr>
</thead>
