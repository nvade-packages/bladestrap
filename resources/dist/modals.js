function openModal(id, routeName, params = []) {
    bootstrap.Modal.getOrCreateInstance('#' + id, {
        keyboard: true,
        focus: true,
        backdrop: true,
    }).show();
};

function modal_agree_action(id, routeName, method, params = []) {
    const button = $('#' + id + '-btn-accept');
    button.find('.d-none').removeClass('d-none').addClass('d-inline-block');
    button.addClass('disabled').prop('disabled', true);
    return axios.request({
        url: route(routeName, params),
        method: method.toUpperCase(),
    }).then((res) => {
        window.location.replace(res.request.responseURL);
    });
}
