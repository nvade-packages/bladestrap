<td {{ $attributes->merge(['align-middle']) }} >
  {{ $slot }}
</td>
