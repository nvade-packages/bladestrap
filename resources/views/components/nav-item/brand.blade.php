@props([
    'href' => url('/'),
])

<a  {{ $attributes->merge(['class' => 'navbar-brand']) }}
    href="{{ $href }}">
  {{ $slot }}
</a>
