@php
$i = 1;
$slots = [];
while(isset(${'slot_' . $i})) {
    $slots[]['content'] = ${'slot_' . $i};
    $i++;
}
$items = array_merge($items, $slots)
@endphp

<div {!! $attributes->merge($attrs) !!} data-bs-ride="carousel">
  @if($indicators == true && count($items) > 1)
    <ol class="carousel-indicators">
      @for($i = 0; $i < count($items); $i++)
        <li data-bs-target="#{{ $attrs['id'] }}" data-bs-slide-to="{{ $i }}"{!! $i == 0 ?  ' class="active"' : '' !!}></li>
      @endfor
    </ol>
  @endif

  <div class="carousel-inner">
    @foreach($items as $item)
      <div class="carousel-item{{ $loop->first ? ' active' : '' }}">
        @isset($item['image']['src'])
          <x-bladestrap-image
            :all="$item['image']['all'] ?? []"
            :class="$item['image']['class'] ?? 'd-block w-100'"
            :src="$item['image']['src']"
            :lazyload="$item['image']['lazyload']"
            :alt="$item['image']['alt'] ?? ''"
          />
        @endisset
        @isset($item['content'])
          {!! $item['content'] !!}
        @endisset
      </div>
    @endforeach
  </div>

  @if($control == true && count($items) > 1)
    <button class="carousel-control-prev" type="button" data-bs-target="#{{ $attrs['id'] }}" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#{{ $attrs['id'] }}" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  @endif
</div>
