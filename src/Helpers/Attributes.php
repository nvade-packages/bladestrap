<?php

namespace Nvade\Bladestrap\Helpers;

class Attributes
{
    public static function get($items, $excludes = [])
    {
        $attrs = '';
        foreach ($items as $key => $value) {
            if (!\in_array($key, $excludes, true) && !empty($value) && !\is_array($value)) {
                $attrs .= ' ' . $key . '="' . $value . '"';
            }
        }

        return $attrs;
    }
}
