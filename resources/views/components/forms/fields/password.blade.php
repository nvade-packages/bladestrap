<x-bladestrap-forms.input :group="['class' => 'mb-3']"
                      name="password"
                      type="password"
                      :label="['text' => __('Wachtwoord')]"
                      placeholder="Vul uw wachtwoord in"
                      :grid="['col-md-4 text-end', 'col-md-8']"
/>
