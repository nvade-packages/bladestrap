<?php

namespace Nvade\Bladestrap\Components\NavItem;

use Illuminate\Http\Request;
use Illuminate\View\Component;

class Index extends Component
{
    public function __construct(
        public Request $request,
        public string $href
    ) {
    }

    public function render(): string
    {
        return view('bladestrap::components.nav-item.index');
    }

    public function isActive(): bool
    {
        return $this->request->is($this->href);
    }
}
