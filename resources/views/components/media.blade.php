<div {!! $attributes->merge($attrs) !!}{!! $attrs2 !!}>
  @if(isset($image['src']))
    <bladestrap-image :all="$image"/>
  @elseif(isset($icon))
    @icon($icon . $mediaObjectClass ?? '')
  @endif

  {!! $object ?? '' !!}

  <div{!! $body['attrs'] !!}>
    @if(!empty($headline))
      <bladestrap-headline :all="$headline"/>
    @endif

    {!! $text ?? '' !!}
    {!! $slot ?? '' !!}
  </div>
</div>
