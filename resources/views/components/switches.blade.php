@php
  $attr = $attributes->merge($attrs);
  if (isset($errors)) {
    $attr['class'] .= $errors->has($attr['name']) ? ' is-invalid' : '';
  }
@endphp

@if(!empty($group['attrs']))
  <div{!! $group['attrs'] !!}>
@endif
  @isset($grid[0])
    <div class="{{ $grid[0] }}">
      @endisset

      <div class="form-check">
        <input value="{{ request()->input($attr['name'], old($attr['name'])) }}" {!! $attr->merge($attrs) !!} type="checkbox" id="checkbox-{{ $attr['name'] }}">

        @if(!empty($label['text']))
          <label{!! $label['attrs'] !!} for="checkbox-{{ $attr['name'] }}">{!! $label['text'] ?? '' !!}</label>
        @endif

        @if(isset($errors) && $errors->has($attr['name']))
          <div class="{{ $errors->has($attr['name']) ? 'invalid' : '' }}-feedback d-block">
            {!! $errors->first($attr['name']) !!}
          </div>
        @endif
      </div>

      @isset($grid[0])
    </div>
  @endisset
@if(!empty($group['attrs']))
  </div>
@endif
