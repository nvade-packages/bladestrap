@props([
    'href' => '#',
    'ariaExpanded' => false,
])
<a {{ $attributes->merge(['class' => 'nav-link dropdown-toggle', 'role' => 'button', 'data-bs-toggle' => 'dropdown']) }}
  href="{{ $href }}"
  aria-expanded="{{ $ariaExpanded }}"
>
  {{ $slot }} <span class="caret"></span>
</a>
