<?php

namespace Nvade\Bladestrap\Components;

use Illuminate\View\Component;
use Nvade\Bladestrap\Helpers\Classes;


class JadeNav extends Component
{
    public $all;

    public $links;

    public $attrs;

    public function __construct(
        $all = [],
        $links = [],
        $class = ''
    ) {
        $this->all = $all ?? '';
        $this->links = $links ?: $all['links'] ?? '';
        $this->attrs = [
            'class' => $class ?: $all['class'] ?? '',
        ];
        $this->attrs['class'] = Classes::get([
            'jade-nav',
            $this->attrs['class'],
        ]);
        $this->attrs = array_filter($this->attrs);
    }

    public function render()
    {
        return view('bladestrap::components.jade-nav');
    }
}
