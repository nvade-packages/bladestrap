<div {!! $attributes->merge($attrs) !!}>
  {!! $message !!}

  {{ $slot }}

  @if($dismissible == true)
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  @endif
</div>
