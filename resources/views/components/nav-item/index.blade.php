<li {{ $attributes->merge(['class' => 'nav-item']) }}>
    <a @class(['nav-link', 'active' => $isActive ]) @if($isActive) aria-current="page" @endif href="{{ $href }}">
        {{ $slot }}
    </a>
</li>
