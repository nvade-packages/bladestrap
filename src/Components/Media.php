<?php

namespace Nvade\Bladestrap\Components;

use Illuminate\View\Component;
use Nvade\Bladestrap\Helpers\Attributes;
use Nvade\Bladestrap\Helpers\Classes;

use function array_replace_recursive;


class Media extends Component
{
    public $attrs;

    public $attrs2;

    public $all;

    public $image;

    public $text;

    public $excerpt;

    public $body;

    public $headline;

    public function __construct(
        $all = [],
        $class = '',
        $image = [],
        $excerpt = [],
        $body = [],
        $text = '',
        $headline = []
    ) {
        $this->all = $all ?? [];
        $this->excerpt = $excerpt ?: $all['excerpt'] ?? [];
        $this->headline = $headline ?: $all['headline'] ?? [];
        $this->image = $image ?: $all['image'] ?? [];
        $this->body = $body ?: $all['body'] ?? [];
        $this->text = $text ?: $all['text'] ?? '';
        $this->attrs2 = Attributes::get($all ?? [], [
            'class',
            'body',
            'text',
            'image',
            'headline',
        ]);
        $this->attrs['class'] = Classes::get([
            'd-flex',
            ['class' => $class ?: $all['class'] ?? ''],
        ]);
        $this->headline['class'] = $this->headline['class'] ?? '';
        $this->image['class'] = Classes::get([
            'flex-shrink-0',
            $this->image['class'] ?? '',
        ]);
        $this->body['class'] = Classes::get([
            'flex-grow-1',
            $this->body['class'] ?? 'ms-3',
        ]);
        $this->body['attrs'] = Attributes::get($this->body);
        $this->attrs = array_filter($this->attrs);

        $merge = ['image', 'headline', 'excerpt'];
        foreach ($merge as $item) {
            $this->$item = array_replace_recursive(
                $all[$item] ?? [],
                $this->$item ?? []
            );
        }

        if (isset($this->excerpt['show']) && isset($this->excerpt['text'])) {
            $this->text = $this->excerpt['text'];
        }
    }

    public function render()
    {
        return view('bladestrap::components.media');
    }
}
