<?php

namespace Nvade\Bladestrap;

use Nvade\Bladestrap\Assets\AssetManager;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Jenssegers\Agent\Agent;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Nvade;

class BladestrapServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package->name('bladestrap')
            ->hasConfigFile()
            ->hasViews()
            ->hasViewComponents('bladestrap', ...$this->viewComponents())
            ->hasCommands([
                Nvade\Bladestrap\Commands\AssetsCommand::class
            ]);
    }

    public function packageRegistered()
    {
        $this->app->scoped(
            AssetManager::class,
            fn() => new AssetManager(),
        );
    }

    public function packageBooted(): void
    {
        Facades\BladestrapAsset::register([
            Nvade\Bladestrap\Assets\Js::make('modals', __DIR__.'/../resources/dist/modals.js')
        ], 'nvade/bladestrap');

        Blade::directive('bladestrapScripts', function (string $expression): string {
            return "<?php echo \Nvade\Bladestrap\Facades\BladestrapAsset::renderScripts({$expression}) ?>";
        });

        Blade::directive('bladestrapStyles', function (string $expression): string {
            return "<?php echo \Nvade\Bladestrap\Facades\BladestrapAsset::renderStyles({$expression}) ?>";
        });

        Blade::directive('icon', static function ($expression) {
            return "<span class='<?=".$expression."; ?>'></span>";
        });

        Blade::component('bladestrap::components.table.td', 'td', 'bladestrap');
        Blade::component('bladestrap::components.table.thead.index', 'thead', 'bladestrap');
        Blade::component('bladestrap::components.table.thead.th', 'th', 'bladestrap');
        Blade::component('bladestrap::components.table.tr.index', 'tr', 'bladestrap');
        Blade::component('bladestrap::components.table.tr.th', 'tr-th', 'bladestrap');
        Blade::component('bladestrap::components.nav-item.index', 'nav-item', 'bladestrap');
        Blade::component('bladestrap::components.buttons.modal', 'modal-button', 'bladestrap');

        View::share('agent', new Agent());
    }

    private function viewComponents(): \Illuminate\Support\Collection
    {
        return collect(config('bladestrap.components'))->values();
    }

}
