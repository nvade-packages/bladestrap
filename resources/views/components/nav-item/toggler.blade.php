@props([
    'target' => 'app-navbar-collapse',
])
<button data-bs-target="#{{ $target }}"
        aria-controls="{{ $target }}"
        @unless($attributes->has('aria-expanded')) aria-expanded="false" @endunless
        @unless($attributes->has('aria-label')) aria-label="Toggle navigation" @endunless
        {{ $attributes->merge(['type' => 'button', 'class' => 'navbar-toggler', 'data-bs-toggle' => 'collapse']) }}
>
  <span class="navbar-toggler-icon"></span>
</button>
