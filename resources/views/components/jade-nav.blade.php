<div {!! $attributes->merge($attrs) !!} data-role="jade-nav" data-device="{{ $agent->isMobile() || $agent->isTablet() ? 'mobile' : 'desktop' }}">
  <ul class="jade-nav-root">
    <li class="jade-nav-section">
      @foreach($links as $link)
        <a class="jade-nav-root-link{{ !empty($link['href']) ? '' : ' jade-nav-has-dropdown' }} {{ $link['class'] ?? '' }}" href="{{ $link['href'] ?? '#' }}" data-stripe-dropdown="{{ $link['slot'] ?? '' }}">{!! $link['title'] !!}</a>
      @endforeach
    </li>
  </ul>

  <div class="jade-nav-dropdown-root">
    <div class="jade-nav-dropdown-background">
      <div class="jade-nav-dropdown-background-alt"></div>
    </div>
    <div class="jade-nav-arraw"></div>

    <div class="jade-nav-dropdown-container">
      @php $i = 1 @endphp
      @while(isset(${'slot' . $i}))
        <div class="jade-nav-dropdown-section" data-stripe-dropdown="slot{{ $i }}">
          <div class="jade-nav-dropdown-content left">
            {!! ${'slot' . $i} !!}
          </div>
        </div>
        @php $i++ @endphp
      @endwhile
    </div>
  </div>
</div>
