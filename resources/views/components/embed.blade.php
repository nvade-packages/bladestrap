<div {!! $attributes->merge($attrs) !!}>
  <iframe src="{{ $src }}" {!! $options !!}></iframe>
</div>
