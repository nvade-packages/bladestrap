<?php

use Nvade\Bladestrap\Components;

return [
    'components' => [
        'nav-item.index' => Components\NavItem\Index::class,
        'checkbox' => Components\Forms\Checkbox::class,
        'form' => Components\Forms\Form::class,
        'input' => Components\Forms\Input::class,
        'radio' => Components\Forms\Radio::class,
        'select' => Components\Forms\Select::class,
        'switches' => Components\Forms\Switches::class,
        'text-area' => Components\Forms\Textarea::class,
        'upload' => Components\Forms\Upload::class,
        'alert' => Components\Alert::class,
        'badge' => Components\Badge::class,
        'breadcrumb' => Components\Breadcrumb::class,
        'card' => Components\Card::class,
        'carousel' => Components\Carousel::class,
        'embed' => Components\Embed::class,
        'headline' => Components\Headline::class,
        'image' => Components\Image::class,
        'jadenav' => Components\JadeNav::class,
        'link' => Components\Link::class,
        'media' => Components\Media::class,
        'modal' => Components\Modal::class,
        'pagination' => Components\Pagination::class,
        'svg' => Components\Svg::class,
    ],

    'assets_path' => null,

];
