@props([
    'name',
    'title' => __('modal.title'),
    'body' => '',
    'yes' => __('modal.yes'),
    'no' => __('modal.no'),
    'route',
    'action',
    'method' => 'GET',
    'params' => [],
    'loading' => false,
])
@php
    $id = $name . '-modal';
@endphp

<a id="{{ $name }}-modal-button" {{ $attributes->merge(['class' => 'max-w-fit btn rounded-0'])}}
onclick="openModal(@js($id), @js($route), @js($params))">
    <div class="d-flex pos-none">
        {{ $slot }}
    </div>
</a>

@push('modals')
    <x-bladestrap-modal id="{{ $id }}">
        <x-slot:title>
            <x-bladestrap-headline>{{ $title }}</x-bladestrap-headline>
        </x-slot:title>
        <x-slot:body>
            <p>{{ $body }}</p>
        </x-slot:body>
        <x-slot:footer>
            <button id="{{ $id }}-btn-accept" onclick="modal_agree_action(@js($id), @js($route), @js($method), @js($params))"
                    type="button"
                    class="btn btn-default btn-accept">
                <div class="d-none">
                    <span class="spinner-border spinner-border-sm" aria-hidden="true"></span>
                    <span class="visually-hidden" role="status">Loading...</span>
                </div>
                {{ $yes }}
            </button>
            <button id="{{ $id }}-btn-decline" type="button" class="btn btn-danger btn-reject"
                    data-bs-dismiss="modal">{{ $no }}</button>
        </x-slot:footer>
    </x-bladestrap-modal>
@endpush
