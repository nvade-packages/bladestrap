<tr {{ $attributes->merge(['class' => 'align-middle']) }}>
  {{ $slot }}
</tr>
