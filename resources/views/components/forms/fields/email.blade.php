<x-bladestrap-forms.input :group="['class' => 'mb-3']"
                      name="email"
                      type="email"
                      :label="['text' => __('E-Mailadres')]"
                      placeholder="Vul uw e-mailadres in"
                      :grid="['col-md-4 text-end', 'col-md-8']"
/>
